This is step project students fe-17 group. The app allows manage desk of medical visits - add new, edit existing, and remove chosen one.
To login use:
  email - cards2020@gmail.com
  pass - cards2020

Stack of technologies:
  - npm
  - webpack
  - sass
  - javascript

Project team: 
  - Olha Sylevych
  - Taras Zaslavskiy
  - Oleksandr Lytovchenko

Tasks:
  - Olha Sylevych
    1. classes Modal, ModalLogIn, ModalForm
    2. classes Input, TextArea, Select
    3. styles

  - Taras Zaslavskiy
    1. server requests
    2. search form
    3. basic render
    4. drag and drop 

  - Oleksandr Lytovchenko
    1. classes Visit, VisitDentist, VisitTherapist, VisitCardiologist
    2. classes Form, FormDentist, FormTherapist, FormCariologist
    3. webpack config

Class description:
  - Class Modal with children
      Class Modal creates an object with DOM elements needed for creating a modal window. 
        - method render() put window on a page in container specified within the method, you can't choose it.
      Class ModalLogIn has the same functional extended to their properties.
      Class ModalForm has additional methods:
        - checkAndPutForm(newForm, existForm) - recieve two arguments: 
          1. newForm - DOM element with filled tag <form>; 
          2. existForm - Dom element that already placed in modal window;
          and replace exist form to new. If exist form is not defined - just put new into modal window.
        - selectForm(event) - check what form needed to be created depends on the chosen doctor, receive event object as an argument, so must be called in listeners.
        - ifEditModal(id) - in case that you need to edit exist visit card, this method receives as argument card id and set the same data attribute to the modal window to transfer this data to form, that sends the request to the server.
          
  - Class Form with children
  Class create an object with DOM elements needed to build form
    - method render() - receives as argument DOM element in with form will be appended and render form to the page.
    - method collectData() - collect values in a filled form in an object, where key is form element assignment and value is element value, and return it
    - method isDataFilled() - receive object as argument and check it for empty values, return boolean depends on results, and console.error with missing properties.
    - method submitForm(newCard, modal) - receives two arguments:
      1. Object with a card from the server; 
      2. modal window where form placed; 
      Must be called in case of successful response from the server, create and render visit cards depends on the chosen doctor, and remove the modal window.  
    - method submitEdit() has the same functional that submitForm(), but in case of editing card, replace the old card with a new.
  Children's classes have no methods and just extend constructor properties.

  - Class Visit with children
  The class creates an object with properties defined in the server response object and object with DOM elements to render it.
    - method render() receive as argument DOM element where visit cadr will be placed. In case you need to return the complete DOM element without placing on a page - just not mentioned any arguments.
  Children classes have the same functional extended to their properties and each of them has one additional method.
    - method showMore() - collect additionals properties values into an array and adding them to the card on-page.

  - Class Input
  Recieve in constructor object with input attributes and class name as a string. Method create() build complete input with received attributes and return it.

  - Class TextArea
  Recieve in constructor object with textarea attributes and class name as a string. Method create() build complete textarea with received attributes and return it.

  - Class Select
  Recieve in constructor array with select options. Method create() build complete select with received options and return it.





