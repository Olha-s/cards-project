import Select from "./form_components/Select.js";
import {templates} from "./utils/templates.js";
import ModalForm from "./modal/ModalForm";
import {editCard, deleteCard} from "./CardsAPI.js";
import {render} from "./render.js";

export class Visit {
    constructor({id, status, doctor, purpose, desc, priority, fullName}) {
        this.id = id;
        this.status = status;
        this.doctor = doctor;
        this.purpose = purpose;
        this.desc = desc;
        this.priority = priority;
        this.fullName = fullName;
        this.elem = {
            self: document.createElement("div"),
            fullName: document.createElement("h3"),
            doctor: document.createElement("span"),
            purpose: document.createElement("span"),
            desc: document.createElement("p"),
            priority: document.createElement("span"),
            status: document.createElement("span"),
            showMoreBtn: document.createElement("button"),
            hideBtn: document.createElement("button"),
            select: new Select(templates.cardEdit).create()
        };
    }

    render(parent) {
        this.elem.fullName.textContent = this.fullName;
        this.elem.doctor.textContent = `Доктор: ${this.doctor}`;
        this.elem.purpose.textContent = `Цель визита: ${this.purpose}`;
        this.elem.desc.textContent = `Описание визита: ${this.desc}`;
        this.elem.priority.textContent = `Срочность: ${this.priority}`;
        this.elem.status.textContent = `Статус: ${this.status}`;
        this.elem.showMoreBtn.textContent = "Расширить";
        this.elem.hideBtn.textContent = "Скрыть";
        this.elem.hideBtn.style.display = 'none';

        this.elem.self.classList.add("visit");
        this.elem.fullName.classList.add("visit__head");
        this.elem.doctor.classList.add("visit__text");
        this.elem.purpose.classList.add("visit__text");
        this.elem.desc.classList.add("visit__text");
        this.elem.priority.classList.add("visit__text");
        this.elem.status.classList.add("visit__text");
        this.elem.showMoreBtn.classList.add("visit__btn");
        this.elem.hideBtn.classList.add("visit__btn");

        this.elem.self.draggable = true;
        this.elem.self.dataset.id = this.id;

        this.elem.select.addEventListener("change", async (event) => {
            if (event.target.value === "Редактировать") {
                const form = new ModalForm();
                event.target.value = event.target.childNodes[0].outerText;
                form.ifEditModal(this.id);
                form.render();
            } else if (event.target.value === "Удалить") {
                const response = await deleteCard(this.id).then(r => r.json());

                if (response.status === "Success") {
                    this.elem.self.remove();
                    const renderedVisits = document.querySelectorAll(".visit");

                    if (!renderedVisits || renderedVisits.length === 0) {
                        const noItem = document.createElement('p');
                        noItem.id = "empty";
                        noItem.textContent = "No item has been added";
                        parent.append(noItem);
                    }
                }
            }
        });

        this.elem.self.append(this.elem.fullName, this.elem.doctor, this.elem.showMoreBtn, this.elem.hideBtn, this.elem.select);
    }
}

export class VisitDentist extends Visit {
    constructor({id, status, doctor, purpose, desc, priority, fullName, lastDateVisit}) {
        super({id, status, doctor, purpose, desc, priority, fullName});
        this.lastDateVisit = lastDateVisit;
    }

    render(parent) {
        super.render(parent);
        this.elem.lastDateVisit = document.createElement("span");

        this.elem.lastDateVisit.textContent = `Дата последнего визита: ${this.lastDateVisit}`;

        this.elem.lastDateVisit.classList.add("visit__text")

        this.elem.showMoreBtn.addEventListener("click", () => {
            this.showMore();
        });
        this.elem.hideBtn.addEventListener("click", () => {
            this.hide();
        });

        if (parent) {
            parent.append(this.elem.self);
        } else {
            return this.elem.self;
        }
    }

    showMore() {
        const moreInfo = [];

        for (let key in this.elem) {
            if (key === "purpose" || key === "desc" || key === "priority" || key === "status" || key === "lastDateVisit") {
                moreInfo.push(this.elem[key]);
            }
        }

        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.showMoreBtn);
        });

        this.elem.showMoreBtn.style.display = 'none';
        this.elem.hideBtn.style.display = 'inline-block';
    }
    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.desc);
        this.elem.self.removeChild(this.elem.priority);
        this.elem.self.removeChild(this.elem.status);
        this.elem.self.removeChild(this.elem.lastDateVisit);
        this.elem.hideBtn.style.display = 'none';
        this.elem.showMoreBtn.style.display = 'inline-block';
    }
}



export class VisitTherapist extends Visit {
    constructor({id, status, doctor, purpose, desc, priority, fullName, age}) {
        super({id, status, doctor, purpose, desc, priority, fullName});
        this.age = age;
    }

    render(parent) {
        super.render(parent);
        this.elem.age = document.createElement("span");

        this.elem.age.textContent = `Возраст: ${this.age}`;

        this.elem.age.classList.add("visit__text")

        this.elem.showMoreBtn.addEventListener("click", () => {
            this.showMore();
        });
        this.elem.hideBtn.addEventListener("click", () => {
            this.hide();
        });

        if (parent) {
            parent.append(this.elem.self);
        } else {
            return this.elem.self;
        }
    }

    showMore() {
        const moreInfo = [];

        for (let key in this.elem) {
            if (key === "purpose" || key === "desc" || key === "priority" || key === "status" || key === "age") {
                moreInfo.push(this.elem[key]);
            }
        }

        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.showMoreBtn);
        });

       // this.elem.self.removeChild(this.elem.showMoreBtn);
        this.elem.showMoreBtn.style.display = 'none';
        this.elem.hideBtn.style.display = 'inline-block';
    }
    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.desc);
        this.elem.self.removeChild(this.elem.priority);
        this.elem.self.removeChild(this.elem.status);
        this.elem.self.removeChild(this.elem.age);
        this.elem.hideBtn.style.display = 'none';
        this.elem.showMoreBtn.style.display = 'inline-block';
    }
}

export class VisitCardiologist extends Visit {
    constructor({id, status, doctor, purpose, desc, priority, fullName, pressure, weightIndex, heartIllness, age}) {
        super({id, status, doctor, purpose, desc, priority, fullName});
        this.pressure = pressure;
        this.weightIndex = weightIndex;
        this.heartIllness = heartIllness;
        this.age = age;
    }

    render(parent) {
        super.render(parent);
        this.elem.pressure = document.createElement("span");
        this.elem.weightIndex = document.createElement("span");
        this.elem.heartIllness = document.createElement("span");
        this.elem.age = document.createElement("span");

        this.elem.pressure.textContent = `Давление: ${this.pressure}`;
        this.elem.weightIndex.textContent = `Индекс массы тела: ${this.weightIndex}`;
        this.elem.heartIllness.textContent = `Ранее перенесенные заболевания сердца: ${this.heartIllness}`;
        this.elem.age.textContent = `Возраст: ${this.age}`;

        this.elem.pressure.classList.add("visit__text");
        this.elem.weightIndex.classList.add("visit__text");
        this.elem.heartIllness.classList.add("visit__text");
        this.elem.age.classList.add("visit__text");

        this.elem.showMoreBtn.addEventListener("click", () => {
            this.showMore();
        });
        this.elem.hideBtn.addEventListener("click", () => {
            this.hide();
        });

        if (parent) {
            parent.append(this.elem.self);
        } else {
            return this.elem.self;
        }
    }

    showMore() {
        const moreInfo = [];

        for (let key in this.elem) {
            if (key === "purpose" || key === "desc" || key === "priority" || key === "status" || key === "pressure" || key === "weightIndex" || key === "heartIllness" || key === "age") {
                moreInfo.push(this.elem[key]);
            }
        }

        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.showMoreBtn);
        });

        this.elem.showMoreBtn.style.display = 'none';
        this.elem.hideBtn.style.display = 'inline-block';
    }
    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.desc);
        this.elem.self.removeChild(this.elem.priority);
        this.elem.self.removeChild(this.elem.status);
        this.elem.self.removeChild(this.elem.pressure);
        this.elem.self.removeChild(this.elem.weightIndex);
        this.elem.self.removeChild(this.elem.heartIllness);
        this.elem.self.removeChild(this.elem.age);
        this.elem.hideBtn.style.display = 'none';
        this.elem.showMoreBtn.style.display = 'inline-block';
    }
}

