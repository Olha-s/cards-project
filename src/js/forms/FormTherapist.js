import Input from "../form_components/Input.js"
import Select from "../form_components/Select.js"
import TextArea from "../form_components/TextArea.js"
import Form from "./Form.js"
import {templates} from "../utils/templates.js"

export default class FormTherapist extends Form {
    constructor(doctor) {
        super(doctor)
        this.age = new Input(templates.age, "form__input").create();
    }

    render(modal) {
        super.render(modal);

        this.self.insertBefore(this.age, this.submit);
    }
}