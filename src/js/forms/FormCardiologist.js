import Input from "../form_components/Input.js"
import Select from "../form_components/Select.js"
import TextArea from "../form_components/TextArea.js"
import {templates} from "../utils/templates.js"
import Form from "./Form.js"

export default class FormCardiologist extends Form {
    constructor(doctor) {
        super(doctor);
        this.pressure = new Input(templates.pressure, "form__input").create();
        this.weightIndex = new Input(templates.weightIndex, "form__input").create();
        this.heartIllness = new TextArea(templates.illness, "form__input").create();
        this.age = new Input(templates.age, "form__input").create();
    }

    render(modal) {
        super.render(modal);

        const nodes = [this.pressure, this.weightIndex, this.heartIllness, this.age];

        nodes.forEach(node => {
            this.self.insertBefore(node, this.submit);
        })
    }
}

