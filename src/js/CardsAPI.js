import {getVisits} from "./search";

export const API = 'https://cards.danit.com.ua';
import {token} from "./modal/ModalLogIN.js";
/////////////////////////////////////////////////////////////
// token="446c8243d46d"


export function getLogin(email, password) {
  return fetch(`${API}/login`,{
    method: "POST",
    body: JSON.stringify({
      email: email,
      password: password
    })
  })
}

export function createCard(card) {
  return fetch(`${API}/cards`,{
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(card)
  })
}

export function deleteCard(id) {
  return fetch(`${API}/cards/${id}`,{
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
}

export function editCard(newCard, cardId) {
  return fetch(`${API}/cards/${cardId}`,{
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(newCard)
  })
}

export function getCards() {
  return fetch(`${API}/cards`,{
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
}



//пара функций, если захочется что то поудалять с сервера

/// - удалить карточки без поля тайтл
function deleteEmpty(cards) {
  console.log(cards);

  cards.then(c => c.forEach((e) => {
    if (e.doctor === undefined) {
      console.log(e.id);
      deleteCard(e.id);
    }
  }))
}

// deleteEmpty(cards1);


export function deleteFirstNElements(n) { // удалить первые N элементов с сервера
  let visitsServer = getCards().then(c => c.json())
    .then(c => {
      for (let i = 0; i < n; i++) {
        deleteCard(c[i].id);
      }
    })
}

// deleteFirstNElements(32);


// кусок тестового кода для визита, не трогайте плз
// const renderedVisits = document.querySelectorAll(".visit"),
//     serverVisits = await getVisits();
// renderedVisits.forEach(renderedVisit => {
//   serverVisits.forEach(visit => {
//     if (renderedVisit.id !== visit.id) {
//       visit.render();
//     }
//   })
// })

