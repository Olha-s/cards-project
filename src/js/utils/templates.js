export const templates = {
    fullName: {
        type: "text",
        placeholder: "ФИО",
        isRequired: true
    },
    purpose: {
        type: "text",
        placeholder: "Цель визита",
        isRequired: true
    },
    desc: {
        placeholder: "Описание визита",
        isRequired: true
    },
    pressure: {
        type: "text",
        placeholder: "Обычное давление",
        isRequired: true
    },
    weightIndex: {
        type: "text",
        placeholder: "Индекс массы тела",
        isRequired: true
    },
    illness: {
        placeholder: "Ранее перенесённые заболевания сердца",
        isRequired: true
    },
    age: {
        type: "text",
        placeholder: "Возраст",
        isRequired: true
    },
    lastDateVisit: {
        type: "text",
        placeholder: "Дата последнего визита",
        isRequired: true
    },
    submit: {
        type: "submit"
    },
    priority: [
        "Выберите срочность",
        "Обычная",
        "Приоритетная",
        "Неотложная"
    ],
    cardEdit: [
        "Опции редактирования",
        "Редактировать",
        "Удалить",
    ],
    status: [
        "Выберите статус",
        "Открыт",
        "Закрыт"
    ]

}