import {getCards} from "./CardsAPI";
import {VisitCardiologist, VisitDentist, VisitTherapist} from "./Visit";

export function render(container) {
  getCards().then(c => c.json())
    .then(visits => {
      console.log(visits);

      // visits = []; - раскомментировать чтобы протестить No item has been added

      if(visits.length === 0){
        const noItem = document.createElement('p');
        noItem.innerText = "No item has been added";
        noItem.id = "empty";
        container.append(noItem);
      }else {
        let visitsObjects = visits.map(v => {
          if (v.doctor === "Стоматолог") {
            const vo = new VisitDentist(v);
            vo.render(container);
            return vo;
          } else if (v.doctor === "Кардиолог") {
            const vo = new VisitCardiologist(v);
            vo.render(container);
            return vo;
          } else if (v.doctor === "Терапевт") {
            const vo = new VisitTherapist(v);
            vo.render(container);
            return vo;
          }
        })

        return visitsObjects;
      }

    });

}