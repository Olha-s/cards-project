import FormDentist from "../forms/FormDentist.js";
import FormTherapist from "../forms/FormTherapist.js";
import FormCardiologist from "../forms/FormCardiologist.js";
import Select from "../form_components/Select.js";
import Modal from "./Modal.js";

export default class ModalForm extends Modal {
    constructor() {
        super()
        this.doctor = new Select(["Выберите врача", "Кардиолог", "Стоматолог", "Терапевт"]).create();
    }

    render() {
        super.render();

        this.doctor.addEventListener("change", (event) => {
            this.selectForm(event);
        });

        this.element.btnSubmit.remove();
        this.element.modalWindow.insertBefore(this.doctor, this.element.btnClose);
    }

    checkAndPutForm(newForm, existForm) {
        if (existForm) {
            this.element.modalWindow.removeChild(existForm);
            newForm.render(this.element.modalWindow);
        } else {
            newForm.render(this.element.modalWindow);
        }
    }

    selectForm(event) {
        const exist = this.element.modalWindow.children[2];

        if (event.target.value === "Кардиолог") {
            const form = new FormCardiologist(event.target.value);
            this.checkAndPutForm(form, exist);
        } else if (event.target.value === "Стоматолог") {
            const form = new FormDentist(event.target.value);
            this.checkAndPutForm(form, exist);
        } else if (event.target.value === "Терапевт") {
            const form = new FormTherapist(event.target.value);
            this.checkAndPutForm(form, exist);
        }
    }

    ifEditModal(id) {
        this.element.modalWindow.dataset.id = id;
        this.element.modalWrapper.dataset.id = id;
    }
}

const btn = document.querySelector(".header__btn-create");
btn.addEventListener("click", () => {
    const modal = new ModalForm();
    modal.render();
})