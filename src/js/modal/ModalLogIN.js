//данные логина, пароль и токен, чтобы удалить лишний файлик регистрэйшн
// cards2020@gmail.com
// cards2020
// 446c8243d46d


import Modal from "./Modal.js";

import {createCard, deleteCard, getCards, getLogin} from "../CardsAPI.js";

import {render} from "../render";

// это создается контейнер, чтобы рендеру было куда рендерить, потом можно рендерить в любое другое место. или передать другой контейнер
const container = document.getElementById("container");


export let token;
token = sessionStorage.getItem('token');

const btnLog = document.querySelector('.header__btn');
btnLog.addEventListener('click', createModal);


window.onload = function () {
    if (token) {
        btnLog.remove();
        document.querySelector('.header__btn-create').removeAttribute('hidden');
    } else {
        btnLog.removeAttribute('hidden');
    }
}


class ModalLogIN extends Modal {
    constructor(email, password) {
        super();
        this.email = email;
        this.password = password;

        this.element.title = document.createElement('p');
        this.element.email = document.createElement('input');
        this.element.password = document.createElement('input');
    }

    render() {
        super.render();
        this.element.email.type = 'email';
        this.element.password.type = 'password';
        this.element.email.placeholder = 'enter your email';
        this.element.password.placeholder = 'enter your password';
        this.element.email.setAttribute("required", "");
        this.element.password.setAttribute("required", "");
        this.element.title.textContent = "LOGIN";

        this.element.email.className = "header__modal-input";
        this.element.password.className = "header__modal-input";
        this.element.title.className = "header__modal-text";

        this.element.modalWindow.append(this.element.btnClose, this.element.title, this.element.email, this.element.password, this.element.btnSubmit);

        this.element.btnSubmit.addEventListener('click', async () => {
            let tokenResponse = await getLogin(this.element.email.value, this.element.password.value).then(r=>r.json());
            if ( tokenResponse.status === "Success") {
               sessionStorage.setItem('token', tokenResponse.token);
               token = sessionStorage.getItem('token');
                this.element.modalWrapper.remove();
                btnLog.remove();
                document.querySelector('.header__btn-create').removeAttribute('hidden');

                render(container); // данные с сервера, рендерятся на страницу

            } else {
                alert("Wrong login/password! Try again.")
            }
        })
    }
}


function createModal() {
    let modalLog = new ModalLogIN();
    modalLog.render();
};
