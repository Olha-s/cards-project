import "./CardsAPI"
import {createCard, deleteCard, getCards} from "./CardsAPI";
import {VisitDentist, VisitCardiologist, VisitTherapist} from "./Visit";

const container = document.getElementById("container");

export function search(formContainer, visitContainer) {

  const searchWrap = document.createElement('form');
  const searchInput = document.createElement('input');
  const statusInput = document.createElement('select');
  const statusInput1 = document.createElement('option');
  const statusInput2 = document.createElement('option');
  const statusInput3 = document.createElement('option');
  const priorityInput = document.createElement('select');
  const priorityInput1 = document.createElement('option');
  const priorityInput2 = document.createElement('option');
  const priorityInput3 = document.createElement('option');
  const priorityInput4 = document.createElement('option');
  const buttonInput = document.createElement('input');


  searchInput.placeholder = "Поиск";
  statusInput1.innerText = "Все";
  statusInput2.innerText = "Открыт";
  statusInput3.innerText = "Закрыт";
  statusInput.append(statusInput1, statusInput2, statusInput3);


  priorityInput1.innerText = "Все";
  priorityInput2.innerText = "Обычная";
  priorityInput3.innerText = "Приоритетная";
  priorityInput4.innerText = "Неотложная";
  priorityInput.append(priorityInput1, priorityInput2, priorityInput3, priorityInput4);

  buttonInput.type = "button";
  buttonInput.value = "Поиск";

  searchWrap.className = 'section__form-wrapper';
  searchInput.className = 'section__form-search';
  statusInput.className = 'section__form-select1';
  priorityInput.className = 'section__form-select2';
  buttonInput.className = "section__form-btn";

// e.preventDefault(); предотвратить перезагрузку страницы при нажатии enter в поле поиска
  searchWrap.addEventListener('submit', (e) => {
    e.preventDefault();
    getAndRender();
  });

  searchWrap.append(searchInput, statusInput, priorityInput, buttonInput);

  formContainer.prepend(searchWrap);


  function getAndRender(){
    visitContainer.innerText = "";

    const visits = getVisits(); // получить данные с сервера и преобразовать их в массив готовых для рендера объектов

    visits.then(cards => {

      let cardsSearch = cards.filter(e => {
        let searchContent = e.doctor + " " + e.purpose + " " + e.fullName;

        if (searchContent.toLowerCase().includes(searchInput.value.toLowerCase()) || searchInput.value === "")
          if (statusInput.value === "Все" && priorityInput.value === "Все")
            return true
          else
            return (statusInput.value === e.status && priorityInput.value === e.priority) || (statusInput.value === "Все" && priorityInput.value === e.priority)
              || (priorityInput.value === "Все" && statusInput.value === e.status);

      });
      cardsSearch.forEach((c) => c.render(visitContainer));
    })

  }


  buttonInput.addEventListener('click', (e) => {
    getAndRender();
  })

}

///////////////////////////////////////////////////////////////////////////////////////////
////// - добыть объекты с сервака и преобразовать их в нужный массив, с соответствующим классом доктора
// - вернуть промис с массивом нужных объектов
export function getVisits() {
  return getCards().then(c => c.json())
    .then(visits => {

      if (visits !== undefined) {

        let visitsObjects = visits.map(v => {
          if (v.doctor === "Стоматолог") {
            return new VisitDentist(v);
          } else if (v.doctor === "Кардиолог") {
            return new VisitCardiologist(v);
          } else if (v.doctor === "Терапевт") {
            return new VisitTherapist(v);
          }
        })

        return visitsObjects;
      }

    });
}




