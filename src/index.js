
import "./js/CardsAPI"

import "./js/script"
import "./js/render"

import "./js/search"
import "./js/Visit"

import "./js/forms/Form"
import "./js/modal/Modal"
import "./js/modal/ModalForm"
import "./js/modal/ModalLogIN"
import "./js/forms/FormCardiologist"
import "./js/forms/FormDentist"
import "./js/forms/FormTherapist"
import "./js/utils/templates"
import "./js/form_components/Input"
import "./js/form_components/Select"
import "./js/form_components/TextArea"

import "./js/drag_drop"

import "./scss/main.scss"
